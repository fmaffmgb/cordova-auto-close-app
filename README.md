This plugin helps to close the Cordova app at a given time. This may help for 
programs, which does hold updates locally and performs js updates only when
the user logs in again. Closing the app at nighttime does force a fresh login
next morning.

To use this function just call the method 
cordova.plugins.AutoCloseApp.startAutoCloseApp("HH:mm");
once, when the user performs a login. If the passed time is wrong, the plugin
will assume as default "03:30" as app close time.